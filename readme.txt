SPGL1: Spectral Projected Gradient for L1 minimization
------------------------------------------------------

This SPGL1 toolbox is a port to Scilab of the original SPGL1 Matlab toolbox
developped by E. van den Berg & M. P. Friedlander and available at :
https://www.math.ucdavis.edu/~mpf/spgl1/index.html

The Scilab port has been performed by F. Béreux, based on version v1.9 of
the original SPGL1 toolbox and is released with the same license (GNU General Public License v2).

To use the toolbox from sources
===============================
in the 'toolbox_spgl1' subdir :

exec('builder.sce')
exec('loader.sce')
exec('demos/demo_toolbox_spgl1.sce')

References
==========

The algorithm implemented by SPGL1 is described in the papers :

- E. van den Berg and M. P. Friedlander, "Probing the Pareto frontier
  for basis pursuit solutions", SIAM J. on Scientific Computing,
  31(2):890-912, November 2008

- Sparse optimization with least-squares constraints E. van den Berg
  and M. P. Friedlander, Tech. Rep. TR-2010-02, Dept of Computer
  Science, Univ of British Columbia, January 2010

