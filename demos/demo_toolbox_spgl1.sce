function spgdemo(interactive)
//DEMO  Demonstrates the use of the SPGL1 solver
//
// See also SPGL1.
    
//   demo.m
//   $Id: spgdemo.m 1079 2008-08-20 21:34:15Z ewout78 $
//
//   ----------------------------------------------------------------------
//   This file is part of SPGL1 (Spectral Projected Gradient for L1).
//
//   Copyright (C) 2007 Ewout van den Berg and Michael P. Friedlander,
//   Department of Computer Science, University of British Columbia, Canada.
//   All rights reserved. E-mail: <{ewout78,mpf}@cs.ubc.ca>.
//
//   SPGL1 is free software; you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as
//   published by the Free Software Foundation; either version 2.1 of the
//   License, or (at your option) any later version.
//
//   SPGL1 is distributed in the hope that it will be useful, but WITHOUT
//   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
//   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General
//   Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with SPGL1; if not, write to the Free Software
//   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
//   USA
//   ----------------------------------------------------------------------
    if argn(2) < 1 | isempty(interactive), interactive = %T; end
    
    // Initialize random number generators 
    rand('seed',0);
    
    // Create random m-by-n encoding matrix and sparse vector
    m = 50; n = 128; k = 14;
    [A,Rtmp] = qr(rand(n,m,'normal'),"e");
    A  = A';
    p  = spgl1_randperm(n); p = p(1:k);
    x0 = zeros(n,1); x0(p) = rand(k,1,'normal');
 
    
    // -----------------------------------------------------------
    // Solve the underdetermined LASSO problem for ||x||_1 <= pi:
    //
    //    minimize ||Ax-b||_2 subject to ||x||_1 <= 3.14159...
    //
    // -----------------------------------------------------------
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');
    printf('%% Solve the underdetermined LASSO problem for   \n');
    printf('%%                                               \n');
    printf('%%   minimize ||Ax-b||_2 subject to ||x||_1 <= 3.14159...\n');
    printf('%%                                               \n');
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');

    printf('\nPress <return> to continue ... \n');
    if interactive, halt(); end

    // Set up vector b, and run solver    
    b = A * x0;
    tau = %pi;
    x = spg_lasso(A, b, tau);

    printf(strcat(repmat('-',1,35))+ ' Solution '+ strcat(repmat('-',1,35))+ '\n');
    printf('nonzeros(x) = %i,   ' ...
           +'||x||_1 = %12.6e,   ' ...
           +'||x||_1 - pi = %13.6e\n', ...
            length(find(abs(x)>1e-5)), norm(x,1), norm(x,1)-%pi);
    printf(strcat(repmat('-',1,80))+ '\n');
    printf('\n\n');

    
    // -----------------------------------------------------------
    // Solve the basis pursuit (BP) problem:
    //
    //    minimize ||x||_1 subject to Ax = b
    //
    // -----------------------------------------------------------
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');
    printf('%% Solve the basis pursuit (BP) problem:\n');
    printf('%%                                      \n');
    printf('%%   minimize ||x||_1 subject to Ax = b \n');
    printf('%%                                      \n');
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');

    printf('\nPress <return> to continue ... \n');
    if interactive, halt(); end

    // Set up vector b, and run solver 
    b = A * x0;  // Signal
    opts = spgSetParms('verbosity',1);
    x = spg_bp(A, b, opts);
    
    figure(1); subplot(2,4,1);
    plot(1:n,x,'b', 1:n,x0,'ro');
    legend('Recovered coefficients','Original coefficients');
    title('(a) Basis Pursuit');

    printf(strcat(repmat('-',1,35))+ ' Solution '+ strcat(repmat('-',1,35))+ '\n');
    printf('See figure 1(a).\n');
    printf(strcat(repmat('-',1,80))+ '\n');
    printf('\n\n');

    
    // -----------------------------------------------------------
    // Solve the basis pursuit denoise (BPDN) problem:
    //
    //    minimize ||x||_1 subject to ||Ax - b||_2 <= 0.1
    //
    // -----------------------------------------------------------
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');
    printf('%% Solve the basis pursuit denoise (BPDN) problem:  \n');
    printf('%%                                                  \n');
    printf('%%   minimize ||x||_1 subject to ||Ax - b||_2 <= 0.1\n');
    printf('%%                                                  \n');
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');

    printf('\nPress <return> to continue ... \n');
    if interactive, halt(); end

    // Set up vector b, and run solver
    b = A * x0 + rand(m,1,'normal') * 0.075;
    sigma = 0.10;       // Desired ||Ax - b||_2
    opts = spgSetParms('verbosity',1);
    x = spg_bpdn(A, b, sigma, opts);
    
    figure(1); subplot(2,4,2);
    plot(1:n,x,'b', 1:n,x0,'ro');
    legend('Recovered coefficients','Original coefficients');
    title('(b) Basis Pursuit Denoise');

    printf(strcat(repmat('-',1,35))+ ' Solution '+ strcat(repmat('-',1,35))+ '\n');
    printf('See figure 1(b).\n');
    printf(strcat(repmat('-',1,80))+ '\n');
    printf('\n\n');


    // -----------------------------------------------------------
    // Solve the basis pursuit (BP) problem in COMPLEX variables:
    //
    //    minimize ||z||_1 subject to Az = b
    //
    // -----------------------------------------------------------
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');
    printf('%% Solve the basis pursuit (BP) problem in COMPLEX variables:\n');
    printf('%%                                                    \n');
    printf('%%   minimize ||z||_1 subject to Az = b               \n');
    printf('%%                                                    \n');
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');

    printf('\nPress <return> to continue ... \n');
    if interactive, halt(); end

    // Create partial Fourier operator with rows idx
    idx = spgl1_randperm(n); idx = idx(1:m);
    // inline function
    function [yy] = opA( x, mode )
      yy = partialFourier(idx,n,x,mode);
    endfunction

    // Create sparse coefficients and b = 'A' * z0;
    z0 = zeros(n,1);
    z0(p) = rand(k,1,'normal') + sqrt(-1) * rand(k,1,'normal');
    b = opA(z0,1);
    
    opts = spgSetParms('verbosity',1);
    z = spg_bp(opA,b,opts);
    
    figure(1); subplot(2,4,3);
    plot(1:n,real(z),'b+',1:n,real(z0),'bo', ...
         1:n,imag(z),'r+',1:n,imag(z0),'ro');
    legend('Recovered (real)', 'Original (real)', ...
           'Recovered (imag)', 'Original (imag)');
    title('(c) Complex Basis Pursuit');
    
    printf(strcat(repmat('-',1,35))+ ' Solution '+ strcat(repmat('-',1,35))+ '\n');
    printf('See figure 1(c).\n');
    printf(strcat(repmat('-',1,80))+ '\n');
    printf('\n\n');


    // -----------------------------------------------------------
    // Sample the Pareto frontier at 100 points:
    //
    //    phi(tau) = minimize ||Ax-b||_2 subject to ||x|| <= tau
    //
    // -----------------------------------------------------------
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');
    printf('%% Sample the Pareto frontier at 100 points:\n');
    printf('%%                                              \n');
    printf('%%   phi(tau) = minimize ||Ax-b||_2 subject to ||x|| <= tau\n');
    printf('%%                                              \n');
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');

    printf('\nPress <return> to continue ... \n');
    if interactive, halt(); end
    
    printf('\nComputing sample');
    

    // Set up vector b, and run solver    
    b = A*x0;
    x = zeros(n,1);
    tau = linspace(0,1.05 * norm(x0,1),100);
    phi = zeros(size(tau));
    
    opts = spgSetParms('iterations',1000,'verbosity',0);
    for i=1:length(tau)
        [x,r] = spgl1(A,b,tau(i),[],x,opts);
        phi(i) = norm(r);
        if ~modulo(i,10), printf('...%i',i); end
    end
    printf('\n');
    
    figure(1); subplot(2,4,4);
    plot(tau,phi);
    title('(d) Pareto frontier');
    xlabel('||x||_1'); ylabel('||Ax-b||_2');
    
    printf('\n');
    printf(strcat(repmat('-',1,35))+ ' Solution '+ strcat(repmat('-',1,35))+ '\n');
    printf('See figure 1(d).\n');
    printf(strcat(repmat('-',1,80))+ '\n');
    printf('\n\n');


    // -----------------------------------------------------------
    // Solve
    //
    //    minimize ||y||_1 subject to AW^{-1}y = b
    //    
    // and the weighted basis pursuit (BP) problem:
    //
    //    minimize ||Wx||_1 subject to Ax = b
    //
    // followed by setting y = Wx.
    // -----------------------------------------------------------
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');
    printf('%% Solve                                          \n');
    printf('%%                                                \n');
    printf('%% (1) minimize ||y||_1 subject to AW^{-1}y = b   \n');
    printf('%%                                                \n');
    printf('%% and the weighted basis pursuit (BP) problem:   \n');
    printf('%%                                                \n');
    printf('%% (2) minimize ||Wx||_1 subject to Ax = b        \n');
    printf('%%                                                \n');
    printf('%% followed by setting y = Wx.                    \n');
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');

    printf('\nPress <return> to continue ... \n');
    if interactive, halt(); end

    // Sparsify vector x0 a bit more to get exact recovery
    k = 9;
    x0 = zeros(n,1); x0(p(1:k)) = rand(k,1,'normal');

    // Set up weights w and vector b 
    w     = rand(n,1) + 0.1;       // Weights
    b     = A * (x0 ./ w);         // Signal
    
    // Run solver for both variants
    opts = spgSetParms('iterations',1000,'verbosity',1);
    AW   = A * spgl1_spdiags(1 ./ w,0,n,n);
    x = spg_bp(AW, b, opts);
    x1 = x;                        // Reconstruct solution, no weighting
    
    opts = spgSetParms('iterations',1000,'verbosity',1,'weights',w);
    x = spg_bp(A, b, opts);
    x2 = x .* w;                   // Reconstructed solution, with weighting
    
    figure(1); subplot(2,4,5);
    plot(1:n,x1,'m*',1:n,x2,'b', 1:n,x0,'ro');
    legend('Coefficients (1)','Coefficients (2)','Original coefficients');
    title('(e) Weighted Basis Pursuit');
    
    printf('\n');
    printf(strcat(repmat('-',1,35))+ ' Solution '+ strcat(repmat('-',1,35))+ '\n');
    printf('See figure 1(e).\n');
    printf(strcat(repmat('-',1,80))+ '\n');
    printf('\n\n');


    // -----------------------------------------------------------
    // Solve the multiple measurement vector (MMV) problem
    //
    //    minimize ||Y||_1,2 subject to AW^{-1}Y = B
    //    
    // and the weighted MMV problem (weights on the rows of X):
    //
    //    minimize ||WX||_1,2 subject to AX = B
    //
    // followed by setting Y = WX.
    // -----------------------------------------------------------
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');
    printf('%% Solve the multiple measurement vector (MMV) problem      \n');
    printf('%%                                                          \n');
    printf('%% (1) minimize ||Y||_1,2 subject to AW^{-1}Y = B           \n');
    printf('%%                                                          \n');
    printf('%% and the weighted MMV problem (weights on the rows of X): \n');
    printf('%%                                                          \n');
    printf('%% (2) minimize ||WX||_1,2 subject to AX = B                \n');
    printf('%%                                                          \n');
    printf('%% followed by setting Y = WX.                              \n');
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');

    printf('\nPress <return> to continue ... \n');
    if interactive, halt(); end

    // Initialize random number generator
    rand('seed',0);
    
    // Create problem
    m = 100; n = 150; k = 12; l = 6;
    A = rand(m,n,'normal');
    p = spgl1_randperm(n); p = p(1:k);
    X0= zeros(n,l); X0(p,:) = rand(k,l,'normal');
    
    weights = 3 * rand(n,1) + 0.1;
    W = spgl1_spdiags(1 ./ weights,0,n,n);
    
    B = A*W*X0;
    
    // Solve unweighted version
    opts = spgSetParms('verbosity',1);
    x    = spg_mmv(A*W,B,0,opts);
    x1   = x;
    
    // Solve weighted version
    opts = spgSetParms('verbosity',1,'weights',weights);
    x    = spg_mmv(A,B,0,opts);
    x2   = spgl1_spdiags(weights,0,n,n) * x;
    
    // Plot results
    figure(1); subplot(2,4,6);
    plot(x1(:,1),'b-');
    plot(x2(:,1),'b.');
    plot(X0,'ro');
    plot(x1(:,2:$),'-');
    plot(x2(:,2:$),'b.');
    legend('Coefficients (1)','Coefficients (2)','Original coefficients');
    title('(f) Weighted Basis Pursuit with Multiple Measurement Vectors');

    printf('\n');
    printf(strcat(repmat('-',1,35))+ ' Solution '+ strcat(repmat('-',1,35))+ '\n');
    printf('See figure 1(f).\n');
    printf(strcat(repmat('-',1,80))+ '\n');
    printf('\n\n');

    
    // -----------------------------------------------------------
    // Solve the group-sparse Basis Pursuit problem
    //
    //    minimize    sum_i ||y(group == i)||_2
    //    subject to  AW^{-1}y = b,
    //    
    // with W(i,i) = w(group(i)), and the weighted group-sparse
    // problem
    //
    //    minimize    sum_i w(i)*||x(group == i)||_2
    //    subject to  Ax = b,
    //
    // followed by setting y = Wx.
    // -----------------------------------------------------------
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');
    printf('%% Solve the group-sparse Basis Pursuit problem            \n');
    printf('%%                                                         \n');
    printf('%% (1) minimize    sum_i ||y(group == i)||_2               \n');
    printf('%%     subject to  AW^{-1}y = b,                           \n');
    printf('%%                                                         \n');
    printf('%% with W(i,i) = w(group(i)), and the weighted group-sparse\n');
    printf('%% problem                                                 \n');
    printf('%%                                                         \n');
    printf('%% (2) minimize    sum_i w(i)*||x(group == i)||_2          \n');
    printf('%%     subject to  Ax = b,                                 \n');
    printf('%%                                                         \n');
    printf('%% followed by setting y = Wx.                             \n');
    printf('%% '+ strcat(repmat('-',1,78))+ '\n');

    printf('\nPress <return> to continue ... \n');
    if interactive, halt(); end

    // Initialize random number generator
    rand('seed',2); // 2
    
    // Set problem size and number of groups
    m = 100; n = 150; nGroups = 25; groups = [];
    
    // Generate groups with desired number of unique groups
    while (length(unique(groups)) ~= nGroups)
       groups  = gsort(ceil(rand(n,1) * nGroups),'g','i'); // Sort for display purpose
    end

    // Determine weight for each group
    weights = 3*rand(nGroups,1) + 0.1;
    W       = spgl1_spdiags(1 ./ weights(groups),0,n,n);

    // Create sparse vector x0 and observation vector b
    p   = spgl1_randperm(nGroups); p = p(1:3);
    jdx = spgl1_ismember(groups,p);
    idx = find(jdx);
    x0  = zeros(n,1); x0(idx) = rand(sum(jdx),1,'normal');
    b   = A*W*x0;
    
    // Solve unweighted version
    opts = spgSetParms('verbosity',1);
    x    = spg_group(A*W,b,groups,0,opts);
    x1   = x;

    // Solve weighted version
    opts = spgSetParms('verbosity',1,'weights',weights);
    x    = spg_group(A,b,groups,0,opts);
    x2   = spgl1_spdiags(weights(groups),0,n,n) * x;
    
    // Plot results
    figure(1); subplot(2,4,7);
    plot(x1);
    plot(x2,'b+');
    plot(x0,'ro');
    legend('Coefficients (1)','Coefficients (2)','Original coefficients');
    title('(g) Weighted Group-sparse Basis Pursuit');

    printf('\n');
    printf(strcat(repmat('-',1,35))+ ' Solution '+ strcat(repmat('-',1,35))+ '\n');
    printf('See figure 1(g).\n');
    printf(strcat(repmat('-',1,80))+ '\n');
    
    
endfunction

spgdemo( %T );
clear spgdemo;
