function x = NormL1_project(x,weights,tau)

if isreal(x)
   // cas réel
   x = oneProjector(x,weights,tau);
else
   // cas complexe
   xa  = abs(x);
   idx = xa < %eps;
   xc  = oneProjector(xa,weights,tau);
   xc  = xc ./ xa; xc(idx) = 0;
   x   = x .* xc;
end
endfunction

