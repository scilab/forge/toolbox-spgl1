function [x,r,g,info] = spg_group( A0, b, groups, sigma, options )
//SPG_GROUP  Solve jointly-sparse basis pursuit denoise (BPDN)
//
// Calling Sequence
// [x, r, g, info] = spg_group(A, b, tau, sigma, x0, options)
//
// Parameters
// A       : (IN) is an m-by-n matrix, explicit or an operator.
// b       : (IN) is an m-vector.
// groups  : (IN) is a vector containing the group number of the corresponding index in x
// sigma   : (IN) is a nonnegative scalar (noise); see (BPDN)
// options : (IN) is a structure of options from spgSetParms
// x       : (OUT) is a solution of the problem
// r       : (OUT) is the residual, r = b - Ax
// g       : (OUT) is the gradient, g = -A'r
// info    : (OUT) is a structure with output information
//
// Description
//
//   SPG_GROUP is designed to solve the basis pursuit denoise problem
//
//   (BPDN)  minimize    sum_k ||X_{i : GROUPS(i) = k}||_2
//           subject to  ||A X - B||_2,2 <= SIGMA,
//
//   where A is an M-by-N matrix, B is a vector, GROUPS is vector
//   containing the group number of the corresponding index in X, and
//   SIGMA is a nonnegative scalar.  In all cases below, A can be an
//   explicit M-by-N matrix or matrix-like object for which the
//   operations A*x and A'*y are defined (i.e., matrix-vector
//   multiplication with A and its adjoint.)
//
//   Also, A can be a function handle that points to a function with the
//   signature
//
//   v = A(w,mode)   which returns  v = A *w  if mode == 1;
//                                  v = A'*w  if mode == 2. 
//   
//   X = SPG_GROUP(A,B,G,SIGMA) solves the BPDN problem.  If SIGMA=0,
//   SIGMA=[] or SIGMA is omitted, then the jointly-sparse basis
//   pursuit (BP) problem is solved; i.e., the constraints in the BPDN
//   problem are taken as AX=B.
//
//   X = SPG_GROUP(A,B,G,SIGMA,OPTIONS) specifies options that are set
//   using SPGSETPARMS.
//
//   [X,R,G,INFO] = SPG_GROUP(A,B,GROUPS,SIGMA,OPTIONS) additionally
//   returns the residual R = B - A*X, the objective gradient G = A'*R,
//   and an INFO structure.  (See SPGL1 for a description of this last
//   output argument.)
// Authors
// for the original Matlab implementation :
//  Ewout van den Berg (ewout78@cs.ubc.ca)
//  Michael P. Friedlander (mpf@cs.ubc.ca)
//    Scientific Computing Laboratory (SCL)
//    University of British Columbia, Canada
// for the Scilab port :
//  François Béreux (francois.bereux@gmail.com)

// --------------------------------------
// Original header below
// --------------------------------------
//
//SPG_GROUP  Solve jointly-sparse basis pursuit denoise (BPDN)
//
//   SPG_GROUP is designed to solve the basis pursuit denoise problem
//
//   (BPDN)  minimize    sum_k ||X_{i : GROUPS(i) = k}||_2
//           subject to  ||A X - B||_2,2 <= SIGMA,
//
//   where A is an M-by-N matrix, B is a vector, GROUPS is vector
//   containing the group number of the corresponding index in X, and
//   SIGMA is a nonnegative scalar.  In all cases below, A can be an
//   explicit M-by-N matrix or matrix-like object for which the
//   operations A*x and A'*y are defined (i.e., matrix-vector
//   multiplication with A and its adjoint.)
//
//   Also, A can be a function handle that points to a function with the
//   signature
//
//   v = A(w,mode)   which returns  v = A *w  if mode == 1;
//                                  v = A'*w  if mode == 2. 
//   
//   X = SPG_GROUP(A,B,G,SIGMA) solves the BPDN problem.  If SIGMA=0,
//   SIGMA=[] or SIGMA is omitted, then the jointly-sparse basis
//   pursuit (BP) problem is solved; i.e., the constraints in the BPDN
//   problem are taken as AX=B.
//
//   X = SPG_GROUP(A,B,G,SIGMA,OPTIONS) specifies options that are set
//   using SPGSETPARMS.
//
//   [X,R,G,INFO] = SPG_GROUP(A,B,GROUPS,SIGMA,OPTIONS) additionally
//   returns the residual R = B - A*X, the objective gradient G = A'*R,
//   and an INFO structure.  (See SPGL1 for a description of this last
//   output argument.)
//
//   See also spgl1, spgSetParms, spg_bp, spg_lasso.

//   Thanks to Aswin Sankaranarayanan for pointing out performance
//   issues with an earlier version of the group preprocessing code.

//   Copyright 2008, Ewout van den Berg and Michael P. Friedlander
//   http://www.cs.ubc.ca/labs/scl/spgl1
//   $Id$

if ~mtlb_exist('options','var'), options = []; end
if ~mtlb_exist('sigma','var') | isempty(sigma), sigma = 0; end
if ~mtlb_exist('groups','var') | isempty(groups)
    error('Third argument cannot be empty.');
end
if ~mtlb_exist('b','var') | isempty(b)
    error('Second argument cannot be empty.');
end
if ~mtlb_exist('A0','var') then
    error('First argument cannot be empty.');
else
  if ( type(A0) ~= 13 ) then
    if isempty(A0) then
      error('First argument cannot be empty.');
    end
  end
end

// Preprocess the groups, normalize numbering
g = groups(:);
n = length(g);
[gidx,idx1,idx2] = spgl1_unique(g);
groups = spgl1_sparse(idx2',1:n,ones(1,n),length(gidx),n);

// Set projection specific functions
// inline functions
function [xx] = NormGroupL2_project_part( x, weight, tau )
   xx = NormGroupL2_project(groups,x,weight,tau);
endfunction

function [xx] = NormGroupL2_primal_part( x, weight )
   xx = NormGroupL2_primal(groups,x,weight);
endfunction

function [xx] = NormGroupL2_dual_part( x, weight )
   xx = NormGroupL2_dual(groups,x,weight);
endfunction

options.project     = NormGroupL2_project_part;
options.primal_norm = NormGroupL2_primal_part;
options.dual_norm   = NormGroupL2_dual_part;

tau = 0;
x0  = [];
[x,r,g,info] = spgl1(A0,b,tau,sigma,x0,options);

endfunction
//
