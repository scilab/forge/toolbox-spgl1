function p = NormL1NN_primal(x,weights)
// Non-negative L1 gauge function

p = norm(x.*weights,1);
if or(x < 0) p = %inf; end;

endfunction


