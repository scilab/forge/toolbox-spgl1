function [x] = spgl1_randperm(n)
  x = grand(1, "prm", (1:n)')';
endfunction
