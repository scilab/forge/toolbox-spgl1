function x = NormGroupL2_project(groups,x,weights,tau)
// Projection binary group matrix

// Compute two-norms of rows
if isreal(x)
   xa  = sqrt(sum(groups * x.^2,"c"));
else
   xa  = sqrt(sum(groups * abs(x).^2,"c"));
end

// Project one one-norm ball
idx = xa < %eps;
xc  = oneProjector(xa,weights,tau);
xa(idx) = 1.; //FB : pour éviter une division par 0 dans le scaling

// Scale original
xc  = xc ./ xa; xc(idx) = 0;
x   = full(groups' * xc).*x;

endfunction
//
