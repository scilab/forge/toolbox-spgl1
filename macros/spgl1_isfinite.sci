function [b] = spgl1_isfinite(A)
   b = abs(A) < %inf
endfunction
