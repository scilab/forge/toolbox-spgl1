function [x,r,g,info] = spg_lasso(A,b,tau,options )
//SPG_LASSO  Solve the LASSO problem
//
// Calling Sequence
// [x, r, g, info] = spg_lasso(A, b, tau, options)
//
// Parameters
// A       : (IN) is an m-by-n matrix, explicit or an operator.
// b       : (IN) is an m-vector.
// tau     : (IN) is a nonnegative scalar; see (LASSO).
// options : (IN) is a structure of options from spgSetParms
// x       : (OUT) is a solution of the problem
// r       : (OUT) is the residual, r = b - Ax
// g       : (OUT) is the gradient, g = -A'r
// info    : (OUT) is a structure with output information
//
// Description
//
//   SPG_LASSO is designed to solve the LASSO problem
//
//   (LASSO)  minimize  ||AX - B||_2  subject to  ||X||_1 <= tau,
//
//   where A is an M-by-N matrix, B is an M-vector, and TAU is a
//   nonnegative scalar.  In all cases below, A can be an explicit M-by-N
//   matrix or matrix-like object for which the operations  A*x  and  A'*y
//   are defined (i.e., matrix-vector multiplication with A and its
//   adjoint.)
//
//   Also, A can be a function handle that points to a function with the
//   signature
//
//   v = A(w,mode)   which returns  v = A *w  if mode == 1;
//                                  v = A'*w  if mode == 2. 
//   
//   X = SPG_LASSO(A,B,TAU) solves the LASSO problem.
//
//   X = SPG_LASSO(A,B,TAU,OPTIONS) specifies options that are set using
//   SPGSETPARMS.
//
//   [X,R,G,INFO] = SPG_LASSO(A,B,TAU,OPTIONS) additionally returns the
//   residual R = B - A*X, the objective gradient G = A'*R, and an INFO
//   structure.  (See SPGL1 for a description of this last output argument.)
//
// Authors
// for the original Matlab implementation :
//  Ewout van den Berg (ewout78@cs.ubc.ca)
//  Michael P. Friedlander (mpf@cs.ubc.ca)
//    Scientific Computing Laboratory (SCL)
//    University of British Columbia, Canada
// for the Scilab port :
//  François Béreux (francois.bereux@gmail.com)

// --------------------------------------
// Original header below
// --------------------------------------
//
//SPG_LASSO  Solve the LASSO problem
//
//   SPG_LASSO is designed to solve the LASSO problem
//
//   (LASSO)  minimize  ||AX - B||_2  subject to  ||X||_1 <= tau,
//
//   where A is an M-by-N matrix, B is an M-vector, and TAU is a
//   nonnegative scalar.  In all cases below, A can be an explicit M-by-N
//   matrix or matrix-like object for which the operations  A*x  and  A'*y
//   are defined (i.e., matrix-vector multiplication with A and its
//   adjoint.)
//
//   Also, A can be a function handle that points to a function with the
//   signature
//
//   v = A(w,mode)   which returns  v = A *w  if mode == 1;
//                                  v = A'*w  if mode == 2. 
//   
//   X = SPG_LASSO(A,B,TAU) solves the LASSO problem.
//
//   X = SPG_LASSO(A,B,TAU,OPTIONS) specifies options that are set using
//   SPGSETPARMS.
//
//   [X,R,G,INFO] = SPG_LASSO(A,B,TAU,OPTIONS) additionally returns the
//   residual R = B - A*X, the objective gradient G = A'*R, and an INFO
//   structure.  (See SPGL1 for a description of this last output argument.)
//
//   See also spgl1, spgSetParms, spg_bp, spg_bpdn.

//   Copyright 2008, Ewout van den Berg and Michael P. Friedlander
//   http://www.cs.ubc.ca/labs/scl/spgl1
//   $Id: spg_lasso.m 1074 2008-08-19 05:24:28Z ewout78 $

if ~mtlb_exist('options','var'), options = []; end
if ~mtlb_exist('tau','var'), tau = []; end
if ~mtlb_exist('b','var') | isempty(b)
    error('Second argument cannot be empty.');
end
if ~mtlb_exist('A','var') then
    error('First argument cannot be empty.');
else
  if ( type(A) ~= 13 ) then
    if isempty(A) then
      error('First argument cannot be empty.');
    end
  end
end

sigma = [];
x0  = [];
//pause;
[x,r,g,info] = spgl1(A,b,tau,sigma,x0,options);

endfunction
//
