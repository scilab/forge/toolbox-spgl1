function [AinB] = spgl1_ismember(A,B)
   mA   = members(A,B);
   AinB = bool2s(mA~=0);
endfunction
